from dataset import Condition

class Conditions:
    def __init__(self, condition, courses):
        self.condition = condition
        self.courses = courses

    def priority(self, courses, condition):
        for con in condition:
            for i,course in enumerate(courses):
                if course['id'] == con:
                    courses[i]['priority'] = 10
        return courses

    def setPass(self, courses, condition):
        # update passed courses
        for con in condition:
            for i,course in enumerate(courses):
                if course['id'] == con:
                    courses[i]['pass'] = True
        return courses

    def setFail(self,courses, condition):
         # add failedTimes in courses
        for con in condition:
            for i,course in enumerate(courses):
                if course['id'] == con[0]:
                    courses[i].update({'failedTimes': con[1]})
                else:
                    courses[i].update({'failedTimes': 0})
        return courses

    def allowedVaheds(self):
        avrage = self.condition['average']
        if 20 >= avrage >=17:
            return 24
        elif avrage>=12:
            return 20
        else:
            return 14     

    def setCon(self,condition):
        Condition['pass'] = condition['pass']
        Condition['fail'] = condition['fail']
        Condition['average'] = condition['average']
        Condition['priority-course'] = condition['priority-course']

    def Make(self):
        new_courses = self.courses
        condition = self.condition
        # fill Condition in dataset
        self.setCon(condition)
        # set pass flag
        new_courses = self.setPass(new_courses, condition['pass'])
        # set fail flag
        new_courses = self.setFail(new_courses, condition['fail'])
        # update priority flag
        new_courses = self.priority(new_courses, condition['priority-course'])
        return new_courses

