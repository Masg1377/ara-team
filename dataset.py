import pandas as pd
from ast import literal_eval

# prototype of Courses dict
Courses = []
# prototype of Classes 982 List
# (1,8,10) =>> first is day number and second is class start time and third is the end
Classes = []

# condition
Condition = {}

co = pd.read_excel('data/Courses.xlsx')
df = pd.DataFrame(co, columns= ['id','name','unit', 'term', 'requirement', 'need', 'priority', 'pass'])

cl = pd.read_excel('data/Classes.xlsx')
df2 = pd.DataFrame(cl, columns= ['ClassId','CourseId','prof', 'class-time', 'exam-date', 'exam-time'])

con = pd.read_excel('data/Condition.xlsx')
df3 = pd.DataFrame(con, columns= ['pass','fail', 'average', 'priority-course'])

def setDataBase():
    for i in range(len(df)):
        Courses.append({'id':df['id'][i],'name':df['name'][i], 'unit':df['unit'][i], 'term':df['term'][i], 'requirement':literal_eval(df['requirement'][i]), 'need':literal_eval(df['need'][i]),'priority':df['priority'][i], 'pass':df['pass'][i]})
    for i in range(len(cl)):
        Classes.append({'ClassId':df2['ClassId'][i],'CourseId':df2['CourseId'][i], 'prof':df2['prof'][i], 'class-time':literal_eval(df2['class-time'][i]), 'exam-date':df2['exam-date'][i], 'exam-time':literal_eval(df2['exam-time'][i])})
    Condition.update({'pass': literal_eval(df3['pass'][0]), 'fail': literal_eval(df3['fail'][0]), 'average': df3['average'][0], 'priority-course': literal_eval(df3['priority-course'][0])})