from Student import *
from dataset import setDataBase
import random
from Condition import Conditions
from Classes import Classes

setDataBase()

# paasedArray contains id of courses       failedArray contains tuples (id of course, failedTimes)
# we will get conditon from user
condition = {'pass': [], 'fail': [(0,0)], 'average': 20, 'priority-course': []}  
courses = studGenerator(condition)

Cl = Classes(courses)
# get classes witch student can get it 
new_class = Cl.makeClass()

def Target1(classs):
    times = 1
    for i in range(5):
        days =[]
        for cl in classs:
            for time in cl['class-time']:
                if time[0] == i+1:
                    days.append(time)
        days = sorted(days, key = lambda i:i[1]) 
        for index,d in enumerate(days):
            if index+1 < len(days):
                times += days[index+1][1] - d[2]
        print(times)
    
    return 1/times

def Target2(classs):
    count = []
    for i in range(5):
        for cl in classs:
            for time in cl['class-time']:
                if time[0] == i+1:
                   count.append(i+1) 
    count = list(dict.fromkeys(count))
    return 1/len(count)


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def range_intrupt_checker(rng1, rng2): #rng is like (1,3,5)
    if(rng1[0]==rng2[0]):
        if( (rng1[1] <= rng2[1] and rng2[1] < rng1[2]) or (rng1[1] < rng2[2] and rng2[2] < rng1[2]) ):
            return True
    else:
        return False

def time_intrupt_checker (cur_cromosom, class_id):
    result = True
    temp_class = list(filter(lambda item: item['ClassId']== class_id ,new_class))
    temp_class_times = temp_class[0]['class-time']

    for cr in cur_cromosom:
        cur_class = list(filter(lambda item: item['ClassId']== cr[0] ,new_class))
        cur_class_times = cur_class[0]['class-time']
        for it in cur_class_times:
            if(result == False):
                break
            for it2 in temp_class_times:
                if (range_intrupt_checker(it, it2) == True):
                    result = False 
                    break
    return result

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def getUnits(so):
    unit = 0
    for it in new_class:
        for s in so:
            if s[0] == it['ClassId']:
                unit+=it['unit']
    return unit

def getClass(id):
    return list(filter(lambda item: item[1] == id ,solution))

def getIndex(id):
    for i,it in new_class:
        if it['ClassId'] == id:
            return i
    return 1

# check dead state, if nothing can add(check mishe age darsi namoonde add she true barmigardoone)
def checkDead(item,so):
    check = 1
    for it in item:
        if len(getClass(it['CourseId'])) == 0 and time_intrupt_checker(so, it['ClassId']):
            check = 0
    return check
# check mikone age darsi mitoone add she true return mikone 
def checkAdds(item,so,rand):
    check = 1
    for it in so:
        if it[0] == item[rand]['ClassId'] or it[1] == item[rand]['CourseId'] or time_intrupt_checker(so, item[rand]['ClassId']) == False:
            check = 0
    return check

# jahesh ba ehtemal 20 darsad
def mutation(so):
    x = random.randint(0, len(so)-1)
    so.pop(x)
    rand = random.randint(0, len(new_class)-1)
    if len(so) == 0:
        so.append((new_class(rand)['ClassId'],new_class(rand)['CourseId']))
    else:
        checkAdd = checkAdds(new_class,so,rand)  
        check = checkDead(new_class, so) 
        if check:
            checkAdd = 0
        elif checkAdd:
            so.append((new_class(rand)['ClassId'],new_class(rand)['CourseId']))
    return so    

def crossover(so1, so2, max_unit):
    r=random.random()
    if r > 0.8:
        rand = random.randint(1, 2)
        if rand == 1: return mutation(so1) 
        else: return mutation(so2)       
    else:
        unit = getUnits(so1)
        rand = random.randint(0, len(so2)-1)
        index = getIndex(so2[rand][0])
        for i in range(len(so1)-1):
            ex = so1
            ex.pop(i)
            checkAdd = checkAdds(new_class, so1, index)
            check = checkDead(new_class, so1)
            if check:
                checkAdd = 0
            elif checkAdd and unit+new_class[index]['unit'] <= max_unit:
                ex.append((new_class(index)['ClassId'],new_class(index)['CourseId']))
                so1 = ex
        return so1

max_gen = 900
gen_no = 0
items = sorted(new_class, key = lambda i: i['priority'],reverse=True) 
solution = []
unit = 0
Con = Conditions(condition, courses)
while(unit < Con.allowedVaheds()):
    checkAdd = 1
    check = 1
    x = random.randint(0, len(items)-1)
    if len(solution) == 0:
        solution.append((items[x]['ClassId'],items[x]['CourseId']))
        unit+= items[x]['unit']
    else:
        checkAdd = checkAdds(items,solution,x)
# check if nothing can add to solution so break -------------------------     
        check = checkDead(items, solution)
        if check:
            break       
#------------------------------------------------------------------------- 
        if checkAdd:
            solution.append((items[x]['ClassId'],items[x]['CourseId']))
            unit+= items[x]['unit']  

            
