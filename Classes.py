from dataset import Classes as Cl

class Classes:
    def __init__(self, courses):
        self.classes = Cl
        self.courses = courses

    # haras kardan list class ha
    def prune(self, courses):
        classes = self.classes
        new_class = []
        for cl in classes:
            for course in courses:
                if cl['CourseId'] == course['id']:
                    new_class.append(cl)
        return new_class

    # append same course to class
    def FillCLass(self,classes):
        courses = self.courses
        for i,cl in enumerate(classes):
            for course in courses:
                if cl['CourseId'] == course['id']:
                    classes[i].update(course)
        return classes

    def makeClass(self):
        courses = self.courses
        classes = self.prune(courses)
        new_class = self.FillCLass(classes)
        # del key id from classes witch is same at CourseId
        for i,cl in enumerate(new_class):
            new_class[i].pop('id', None)
        
        return new_class
    
        

        