from dataset import Courses
from Condition import Conditions

class Student:
    def __init__ (self,ID,termNum,condition):
        self.ID=ID 
        self.termNum  =termNum
        self.condition = condition.copy()
        #print(self.condition)
        
    def passed(self):
        return list(filter(lambda item: item['pass']== True ,self.condition))
    def failed(self,times):
        return list(filter(lambda item: item['failedTimes'] == times ,self.condition))
    def notPass(self):
        return list(filter(lambda item: item['pass']== False ,self.condition))
        
    # get list of id of requirement if
    def ownPass(self,lists):
        condition = self.passed()
        temp = []
        for li in lists:
            for con in condition:
                if li == con['id']:
                    temp.append(li)
        return temp    
    # get need courses
    def getNeedCourse(self, lists):
        condition = self.condition
        temp = []
        for li in lists:
            for con in condition:
                if li == con['id']:
                    temp.append(con)
        return temp            
    # get a course
    def getCourse(self, id):
        return list(filter(lambda item: item['id'] == id ,self.condition))
    # haras kardan condition
    def prune(self): 
        notPass = self.notPass()
        passed = self.passed()
        new_list = []
        # check pishniaz
        for con in notPass:
            if con['requirement'] == self.ownPass(con['requirement']):
                new_list.append(con)
            else:
                check = True
                for require in con['requirement']:
                    for item in notPass:
                        if (require == item['id']) and item['failedTimes'] == 0:
                            check = False
                        elif (require == item['id']) and item['failedTimes'] > 0:
                            con['need'].append(require) # append requirement to need       
                if check: new_list.append(con) 
        # check karamoozi va proje
        unit = 0
        for it in passed:
            unit += it['unit']
        if unit < 100:
            new_list.remove(self.getCourse(52)[0])
        if unit < 80:
            new_list.remove(self.getCourse(44)[0])        
        # check hamniaz
        lists = new_list
        for li in lists:
            needs = self.getNeedCourse(li['need'])
            for need in needs:
                if need['pass'] == False:
                    result = list(filter(lambda item: item['id'] == need['id'] ,lists))
                    if len(result) == 0:
                       new_list.remove(li)
        return new_list

students = []

def studGenerator(condition):
    Con = Conditions(condition, Courses)
    new_courses = Con.Make()
    stud = Student(96125570,1,new_courses)
    students = stud.prune()
    students = sorted(students, key = lambda i: i['priority'],reverse=True) 
    result = []
    sum_of_unit = 0
    for co in students:
        if co['unit'] + sum_of_unit <= Con.allowedVaheds():
            sum_of_unit += co['unit']
            result.append(co)
    return result

